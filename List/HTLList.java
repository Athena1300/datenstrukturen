package at.dgi.datastructure;

public interface HTLList {

	public void add(int i);
	public void remove(int index);
	public void clear();
	public void get(int index);
}
